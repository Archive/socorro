#
# A mod_python environment for the crash report collector
#

import collect
import config
import xmlrpclib
from mod_python import apache
from mod_python import util
import tempfile
import pprint
import base64
import StringIO

import sys
import os

def makeXMLRPCResponse (response):
	return '<?xml version="1.0"?>\n<methodResponse>\n<params>\n<param>\n<value><string>'+response+'</string></value>\n</param>\n</params>\n</methodResponse>\n'

def handler(req):
  if req.method == "POST":
    try:
      data = req.read(-1)
      (params, method_name) = xmlrpclib.loads(data)
      id = params[0]['minidump-id']
      pprint.pprint (params[0]['minidump-id'], sys.stderr)
      stream = StringIO.StringIO(base64.b64decode(params[0]['minidump'].data))
      (dumpID, dumpPath, dateString) = collect.storeDump(stream)

      form = {}
      form['version'] = params[0]['version']
      form['product'] = params[0]['product']
      form['component'] = params[0]['component']
      form['gnome_version'] = params[0]['gnome_version']
      form['reporter'] = params[0]['reporter']
      form['short_desc'] = params[0]['short_desc']
      form['comment'] = params[0]['comment']
      form['os_version'] = params[0]['os_version']
      collect.storeJSON(dumpID, dumpPath, form)
      req.content_type = "text/plain"
      req.write(makeXMLRPCResponse (collect.makeResponseForClient(dumpID, dateString)))
    except:
      print >>sys.stderr, "Exception: %s" % sys.exc_info()[0]
      print >>sys.stderr, sys.exc_info()[1]
      print >>sys.stderr
      sys.stderr.flush()
      return apache.HTTP_INTERNAL_SERVER_ERROR
    return apache.OK
  else:
    return apache.HTTP_METHOD_NOT_ALLOWED
