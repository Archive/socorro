"""
socorro

This file loads the finished app from socorro.config.middleware.

"""

from socorro.config.middleware import make_app
