#!/bin/bash

ORIG=$1
DEST=$2

for i in $1/*; do 
	test -f $i; if [ $? = 0 ]; then 
		test -L $i; if [ $? = 1 ]; then 
			ID=`file_id $i`
			if [ $? != 0 ]; then
				echo "$i is not a elf file"
			else
				echo "Proccesing $i : $ID"
				dumper $i > $DEST/tmp.sym 
				if [ $? == 0 ]; then
					NAME=`basename "$i"`
					echo "Creating dir $DEST/$NAME/$ID"
					mkdir -p $DEST/$NAME/$ID
					echo "Moving to $DEST/$NAME/$ID/$NAME.sym"
					mv $DEST/tmp.sym $DEST/$NAME/$ID/$NAME.sym
				fi
				
			fi
		fi
	fi
done

