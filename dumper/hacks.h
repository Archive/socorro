#ifndef __HACKS_H__
#define __HACKS_H__

#include <unistd.h>

/* Ugly collection of hacks. Mostlty stuff coming from valgrind
 * to make the port quickly */

typedef  unsigned char   UChar;
typedef    signed char   Char;
typedef           char   HChar; /* signfulness depends on host */
                                /* Only to be used for printf etc 
 *                                    format strings */

/* Always 16 bits. */
typedef  unsigned short  UShort;
typedef    signed short  Short;

/* Always 32 bits. */
typedef  unsigned int    UInt;
typedef    signed int    Int;

/* Always 64 bits. */
typedef  unsigned long long int   ULong;
typedef    signed long long int   Long;

/* Always 128 bits. */
typedef  UInt  U128[4];


typedef  float   Float;    /* IEEE754 single-precision (32-bit) value */
typedef  double  Double;   /* IEEE754 double-precision (64-bit) value */

/* Bool is always 8 bits. */
typedef  unsigned char  Bool;
#define  True   ((Bool)1)
#define  False  ((Bool)0)




typedef unsigned long          UWord;     // 32             64

typedef signed long            Word;      // 32             64

typedef UWord                  Addr;      // 32             64
typedef UWord                  AddrH;     // 32             64

typedef UWord                  SizeT;     // 32             64
typedef  Word                 SSizeT;     // 32             64

typedef  Word                   OffT;     // 32             64

typedef ULong                 Off64T;     // 64             64

Bool string_match ( const Char* pat, const Char* str );

extern HChar* clo_trace_symtab_patt;
extern Bool   clo_trace_cfi;
extern Int    clo_verbosity;
extern Bool   clo_trace_redir;
extern Bool   clo_xml;
extern HChar* clo_xml_user_comment;
extern Bool   clo_debug_dump_syms;
extern Bool   clo_debug_dump_line;
extern Bool   clo_debug_dump_frames;
extern Bool   clo_trace_symtab;
extern Bool   clo_sym_offsets;

static inline Bool toBool ( Int x ) {
   Int r = (x == 0) ? False : True;
   return (Bool)r;
}


#else
#endif
