
#include <string.h>

#include "config.h"

#include "pub_core_debuginfo.h"
#include "hacks.h"

struct _Line {
	int line_number;
	int size;
	Addr addr;
};

typedef struct _Line Line;


static gint
compare_line (gconstpointer a, gconstpointer b)
{
	return ((Line*)a)->line_number - ((Line*)b)->line_number;
}

static gint
compare_line_address (gconstpointer a, gconstpointer b)
{
	return (gint)((Line*)a)->addr - ((Line*)b)->addr;
}



static char*
get_id (const char *path)
{
	gchar *command_line;
	gint exit_status;
	gchar *standard_output = NULL;
	gchar *standard_error = NULL;
	GError *error = NULL;

	command_line = g_strdup_printf ("file_id %s", path);
	
	if (!g_spawn_command_line_sync (command_line, &standard_output, &standard_error,
		 		        &exit_status, &error)) {
		g_free (standard_error);
		g_free (standard_output);
		g_critical ("Couldn't execute file_id: %s\n", error->message);
		g_error_free (error);
	}

	return standard_output;
}
		
static void
print_module_info (char *filename)
{
	char *id;
	char *basename;
	g_print ("MODULE ");
#ifndef VGO_linux
#error "OS Not supported"
#endif
	g_print ("Linux ");
#ifdef VGP_x86_linux
	g_print ("x86 ");
#else
#ifdef VGP_amd64_linux
	g_print ("x86_64 ");
#else
#error "ARCH Not supported"
#endif
#endif
	id = get_id (filename);
	basename = g_path_get_basename (filename);

	g_print ("%s %s\n", id, basename);

	g_free (id);
	g_free (basename);
}



static GSList*
locate_files (SegInfo *si, int syms)
{
	int i;
	GSList *files = NULL;
	char *current_file = NULL;
	Char dirname[255];
	char *full_path;
	Bool got;
	UInt dummyline;



	for (i=0; i<syms; i++) {
		Addr addr, tocptr;
		UInt size;
		HChar *name;
		Char filename[255];

		seginfo_syms_getidx  (si, i, &addr, &tocptr, &size, &name);
		get_filename_linenum(addr, filename, 255, dirname, 255, &got, &dummyline);
		if (got) {
			full_path = g_strdup_printf ("%s/%s", dirname, filename);	
			if (current_file == NULL || strcmp (current_file, full_path)) {
				current_file = full_path;
				files = g_slist_prepend (files, current_file);
			}
		}
				
	}

	files = g_slist_reverse (files);

	return files;
}

void static
print_files (GSList *files)
{
	GSList *tmp = files;
	int i;

	for (i = 0; tmp; i++) {
		g_print ("FILE %d %s\n", i, (char*)tmp->data);
		tmp = tmp->next;
	}
}

void static
free_files (GSList *files)
{
	GSList *tmp = files;
	while (tmp) {
		g_free (tmp->data);
		tmp = tmp->next;
	}
	g_slist_free (files);
}
			

int main (int argc, char *argv[])
{
	int i;
	SegInfo* si;
	GSList *files = NULL;
	int syms;

	g_log_set_always_fatal(G_LOG_LEVEL_CRITICAL);

	if (argc != 2) {
		g_print ("Usage %s <filename>\n", argv[0]);
		_exit(1);
	}

	si = di_notify_mmap (argv[1], 0);
	if (si == NULL) {
		g_critical ("Cannot load filename");
		_exit (2);
	}

	print_module_info (argv[1]);

	syms = seginfo_syms_howmany (si);

	files = locate_files (si, syms);

	print_files (files);

	for (i=0; i<syms; i++) {
		Addr addr, tocptr;
		UInt size;
		HChar *name;
		Char funcname[255];
		Char filename[255];
		Char dirname[255];
		char *full_path;
		Bool got;
		UInt dummyline;

		seginfo_syms_getidx  (si, i, &addr, &tocptr, &size, &name );
		if (get_fnname_if_entry (addr, funcname, 255)) {
			Addr j;
			int file_pos;
			int prev_line = 0;
			GSList *find_file;
			int linsize = 0;

			get_filename_linenum(addr, filename, 255, dirname, 255, &got, &dummyline);
			if (got)
				full_path = g_strdup_printf ("%s/%s", dirname, filename);	
			else {
				/* It seems that the last one is always missing dirname */
				break;
			}
			find_file = g_slist_find_custom (files, full_path, strcmp);
			if (find_file) {
				file_pos = g_slist_index (files, find_file->data);
				g_free (full_path);
			} else {
				g_critical ("FILE %s not found on previous pass!\n", filename);
			}
			g_print ("FUNC %lx %lx %lx %s\n", addr, size, tocptr, name);
			for (j = addr; j < addr + size; j++) {
				int lin;
				if (get_linenum (j, &lin)) {
					linsize++;
					if (prev_line == 0) {
						prev_line = lin;
					} else if (prev_line != lin) {
						g_print ("%lx %lx %d %d\n", j-linsize+1, linsize, prev_line, file_pos);
						linsize = 0;
						prev_line = lin;
					}
				}
			}

		}
	}


	/* Clean memmory */
	free_files (files);
	di_notify_munmap (0,0);


	return 0;
}
