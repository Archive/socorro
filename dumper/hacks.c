#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "hacks.h"

#include "config.h"

HChar* clo_trace_symtab_patt = "*";
Bool   clo_trace_cfi      = False;
Int    clo_verbosity      = 1;
Bool   clo_trace_redir    = False;
Bool   clo_xml            = False;
HChar* clo_xml_user_comment = NULL;
Bool   clo_debug_dump_syms = False;
Bool   clo_debug_dump_line = False;
Bool   clo_debug_dump_frames = False;
Bool   clo_trace_symtab   = False;
Bool   clo_sym_offsets    = False;



/* Keep track of recursion depth. */
static Int recDepth;

// Nb: vg_assert disabled because we can't use it from this module...
static Bool string_match_wrk ( const Char* pat, const Char* str )
{
   //vg_assert(recDepth >= 0 && recDepth < 500);
   recDepth++;
   for (;;) {
      switch (*pat) {
      case '\0':recDepth--;
                return (*str=='\0');
      case '*': do {
                   if (string_match_wrk(pat+1,str)) {
                      recDepth--;
                      return True;
                   }
                } while (*str++);
                recDepth--;
                return False;
      case '?': if (*str++=='\0') {
                   recDepth--;
                   return False;
                }
                pat++;
                break;
      case '\\':if (*++pat == '\0') {
                   recDepth--;
                   return False; /* spurious trailing \ in pattern */
                }
                /* falls through to ... */
      default : if (*pat++ != *str++) {
                   recDepth--;
                   return False;
                }
                break;
      }
   }
}

Bool string_match ( const Char* pat, const Char* str )
{
   Bool b;
   recDepth = 0;
   b = string_match_wrk ( pat, str );
   //vg_assert(recDepth == 0);
   /*
   VG_(printf)("%s   %s   %s\n",
               b?"TRUE ":"FALSE", pat, str);
   */
   return b;
}


Int fsize ( Int fd )
{
   struct stat buf;
   int res;
   res = fstat (fd, (UWord)&buf);
   return res ? (-1) : buf.st_size;
}

void strncpy_safely ( Char* dest, const Char* src, SizeT ndest )
{
   Int i = 0;
   while (True) {
      dest[i] = 0;
      if (src[i] == 0) return;
      if (i >= ndest-1) return;
      dest[i] = src[i];
      i++;
   }
}

